# Thank you for purchasing **OXI One**!

This is the result of a couple musicians in need of a more inspiring
sequencer.

By supporting us, you are helping to keep this instrument up to date and
bring new features for a long period of time and also to develop other
nice devices that will come soon

This is our Opera Prima and we hope you enjoy it as much as we did
developing, broadcasting, manufacturing and shipping it to your door.

· · · · · ·

Along the journey of learning any new instrument, having access to a
complete guide can be a very valuable tool. You can rest assured knowing
that all of the information needed to quickly get the most out of your
OXI One sequencer has been assembled here for convenient referencing.

Exploring a new instrument and manual for the first time can be very fun
and informative indeed, but take your time! There<span
dir="rtl">’</span>s a lot going on in the OXI One and attempting to
master every aspect of it should not be the goal right off the bat. Feel
free to gloss over this manual in its entirety or jump straight to the
chapters that excite you the most. If you<span dir="rtl">’</span>d
prefer to figure things out on your own, no problem! With OXI One,
exploration is encouraged. The important thing is that you<span
dir="rtl">’</span>re having fun! An initial read-through of a manual can
build a strong base of information that will be helpful moving forward.
This is when you<span dir="rtl">’</span>ll first become acquainted with
the parameters of your new instrument; the tools at your disposal and
how they interact with each other.

Regardless of where you are on the journey to mastering the OXI One
sequencer, this manual has something to offer for every user level. Day
1 or day 1,000. Each subsequent read-through has the potential to add
upon the knowledge that you have obtained and invite you to dive in a
bit deeper. Has an update brought new features to explore and
investigate? Perhaps you<span dir="rtl">’</span>d like to refresh your
memory on a feature that you haven<span dir="rtl">’</span>t used often?
Revisiting a manual with a good understanding of the base subject allows
us to interpret the ideas and information within its pages differently,
from a more informed stance. This is when new doors start opening. We
are now using the guide less as a source for information and more as a
source of inspiration.

**ENJOY.**<img src="images/image1.jpeg" style="width:5.82991in;height:2.10869in" />

Safety warnings, warranty coverage, certificates, specs….

**Panel Overview (show picture with numbers of button groupings, add
symbols next to unlabeled buttons bellow)**

**Grid, screen, encoders, transport, function buttons…**

**I/O**<img src="images/image1.png" style="width:6.69298in;height:0.29816in" />

· USB C for power and MIDI USB. It also charges the internal Li battery

· MIDI TRS input and output (Din adapter included)

· MIDI Bluetooth BLE 5.0 bidirectional, peripheral and central roles.
ADD connectivity options.

· CV 1 to 8. From -3V to 8V

· GATE 1 to 8. From 0V to 5V

· Clock Out and Clock/CV In (can be independent with a splitter)

# INDEX

[[_TOC_]]


# VERY QUICK START

OXI One is a battery powered performative sequencer, controller and
composition instrument that has 4 fully independent **Sequencers** that
can be configured to any of the modes available (up to 32 tracks) that
can play simultaneously. The interface in OXI One is based on horizontal
movement between buttons, avoiding nested menus thus making it very
intuitive to use, not only without sacrificing functionality but adding
innovative layers for performance.

Turn **ON** OXI One pressing the button on the left side. To start
playing, press **Play** button indeed. The transport bar will start
moving and loop on the grid. Be sure to have the selected **Sequencer**
(1 to 4 buttons) active. For this, press **Shift** + **1-4**. The number
button will blink indicating the Sequencer is active for playback. This
is a nice way to launch or stop each Sequencer from playback without
stopping the rest.

–––––––––––––––––––––

*You can edit each of the four Sequencers by pressing its number
button.*

–––––––––––––––––––––

Activate notes by pressing pads on the **Piano** **Roll** view on the
grid. In general down to top is lower to higher note, and left to right,
order in the time sequence.

To change between Sequencer, press its button number. The [**<u>Function
buttons</u>**](#function-buttons) and the grid will show the parameters of the selected
Sequencer.

Long press a **Sequencer** button (1-4) to enter its configuration, you
can change the sequencer type between [**<u>Mono</u>**](#mono),
[**<u>Chord</u>**](#chord), [**<u>Polyphonic</u>**](#polyphonic) or
[**<u>Multitrack</u>**](#multitrack); also color for that Sequencer, global MIDI
channel and quantization for punch-in. There will be more exciting modes
to choose from in future updates so please subscribe to [*<u>our
newsletter</u>*](https://oxiinstruments.com) to keep track of them.

Each sequencer type focuses on several task that it does best; for
example, a **Mono** sequencer has an instant visual feedback of the
melody and more compositional centered tools, meanwhile in
**Multitrack** mode you have 8 mono tracks with instant mute and solo
buttons per track. **Chord** mode is a great tool to write the
foundation of a song, meanwhile **Polyphonic** mode offers total freedom
un to 7 notes per step. We’ll cover more in detail how each work in the
[*<u>Sequencer types</u>*](#sequencer-types) section.

–––––––––––––––––––––

*It’s possible to have any combination of the existing* *[<u>sequencer
types</u>](#sequencer-types) on the 4 available slots.*

–––––––––––––––––––––

Turn **OFF** OXI One by holding the power button 2 seconds. It will
remember its last state after startup.

# SIGHT AND TOUCH

–––––––––––––––––––––

*The information displayed by the following elements refers to the
selected Sequencer.*

–––––––––––––––––––––

## GRID

The grid has 128 RGB LED lit pads. On power up, the **Piano Roll** view
is displayed. Each column of pads represent a step of the sequence and
each row represent a note (depending on selected scale) or a track (in
Multitrack mode). The Grid also shows [**<u>Keyboard</u>**](#keyboard),
[**<u>Arranger</u>**](#arranger) and [**<u>CV Out</u>**](#FIX) layouts that will
be explained later.

**·**
## SCREEN
<img src="images/image1.bmp.png" style="width:1.77778in;height:0.88889in" />

The screen shows information or parameter values depending on the
function button or mode selected. The default view is **Sequencer
screen**, showed on power up. From left to right, top to bottom, it
shows: Active sequencer type, playback direction, swing, clock source,
BLE connection, battery state, bpm, Sequencer or track time division,
scale and global octave and track number in [<u>Multitrack</u>](#multitrack).
Specific screen interfaces will be later explained.

–––––––––––––––––––––

***SHIFT.** Hold it and press a button to access secondary functions.*

–––––––––––––––––––––

## SEQUENCERS
1-4**<img src="images/image2.bmp.png" style="width:1.77778in;height:0.88889in" />

Each number selects the displayed sequencer on the grid and the function
buttons will show its parameters. **Shift** + **1-4** activates the
specific sequencer for playback. Long press and hold 1-4 to access
Sequencer Setup menu. From left to right you can change: Global MIDI
channel of the Sequencer (it can be different per track), Punch-In
quantization, Sequencer type (Mono and Chord up to 128 steps, Poly and
Multitrack up to 64 steps) and color for the notes on the grid.

Each **Sequencer** (1 to 4) has their own dedicated interface and global
parameters that are configurable **independently** per Sequencer:

· **Piano Roll** view and pages

· **MIDI** channel configuration (up to 16 tracks or voices on different
channels)

· [**<u>Keyboard</u>**](#keyboard) layout, designed for each mode

· **Tracks**, depending on mode

· [**<u>16 Pattern</u>**](#pattern) slot memory per **Project** (**64** slots
per Project for any sequencer type)

· [**<u>Arranger</u>**](#arranger) track

· **Scale**

· [**<u>Arpeggiator</u>**](#arpeggiator)

· **[<u>Loop</u>](\l)** (per track)

· [**<u>Swing</u>**](\l)

· [**<u>Randomizer</u>**](\l) engine

· [**<u>Initial</u>**](\l) and [**<u>End</u>**](\l) steps (per track)

· **[<u>Time Division</u>](\l)** signature (per track)

· [**<u>CC</u>**](\l) configuration (up to 8)

· [**<u>LFO</u>**](\l)

## ENCODERS 1-4

They are the path to all parameter changes. In Sequencer screen, the
parameters each one modify apply to the selected Sequencer (1-4):

**VELocity** (secondary function: **Tempo**)

**OCTave** (secondary function: **Swing**)

**GATE** (secondary function:
**Scale**)<img src="images/image3.bmp.png" style="width:1.77778in;height:0.88889in" />

**MODulation** (secondary function: **Root**)

For most of the **function** **buttons** and secondary functions, the
screen will show 4 parameters. The encoders modify the corresponding
parameter position on the screen. For that, hold or click the function
button, move the encoders and press the encoder button to select value.

· Most parameters doesn’t requite to be selected by pressing the encoder
button, just release the function button or Shift after highlighting it
on the screen. Some requires confirmation.

· Pressing the encoder button resets the corresponding parameter to its
default value

**Velocity**:

It changes the velocity value for all the steps of the selected
Sequencer or track in Multitrack. It there’s different velocity values
on steps, it shows the minimum and maximum values. Turning the encoder
then, offsets each step value. If there’s an automation on, like vel
randomization or LFO, this parameter works as an offset of the values
automated.

If we press the VEL encoder button while we keep pressed a step button
in the grid, the velocity is set to its default value.

Holding a grid button will show the Step menu, in which you can change
Velocity per step.

Velocity affects the amplitude of the AD [*<u>trig envelope</u>*](\l) in
*[<u>CV Out</u>](\l).*

**Octave**:

By default, the octave is C2. Turn encoder 2 to transpose an octave
up/down ALL the steps on the selected Sequencer.

**Gate**:

Same as velocity, if there’s different gate values on steps, it shows
maximum and minimum value. It acts then as a global control. After gate
= 99 you have TIE and LEGato. Remember you can change this parameter per
step as well.

TIE holds the current note for a number of steps. It can also be
introduced by holding a step on the grid and pressing another step on
the same row depending on the desired length.

LEGato creates a smooth transition between consecutive notes, so there’s
no gap between them.

Gate affects directly the Decay time of the AD [*<u>trig
envelope</u>*](\l) in [*<u>CV Out</u>*](\l).

**MOD**:

Same as velocity and gate, MOD parameter shows minimum and maximum value
if steps have different values. By default MOD sends Mod Wheel messages.
You can change this on the [*<u>CC Perform</u>*](\l) and [*<u>CC
Configuration</u>*](\l) menus later explained.

**Tempo**:

It affects all the Sequencers. To modify it, press Shift + turn encoder
1. The default tempo value can be modified in the desktop app. It only
works when the OXI One is in **INT** (internal) synchronization mode
(see [<u>Synchronization</u>](\l) section).

–––––––––––––––––––––

*Press TAP repeatedly to tune the tempo to the desired beat.*

–––––––––––––––––––––

**Swing**:

Press SHIFT and turn the encoder 2 to modify it. Every sequence has its
own Swing value. Since every sequence may have it's different time
signature or time division, the swing value must be set carefully
according to every sequence in order to get musical results. By pressing
SHIFT and the encoder 2 button, Swing is reset to default value, 50.

**Scale:** 

You can choose among a bunch of different scales, the notes in the
corresponding Sequencer will be quantized to the selected scale. Press
SHIFT and turn encoder 3 to modify it. By pressing SHIFT and the encoder
3 button, scale is set to the default value which is Chromatic.

There’s a very nice feature that we will cover in the
[***<u>HARMONIZER</u>***](\l) mode section, in which notes in a
Sequencer follow harmonically the notes of the chord played on another
Sequencer. This ensures that all your instruments stay not only in key
but inversion too and follow your chord changes.

**Root:**

The default root note is C, but we can easily change it by pressing
SHIFT and turning encoder 4. As with the previous parameters, by
pressing SHIFT and the encoder button, root is set to the default value,
C.

# FUNCTION BUTTONS

<img src="images/image4.bmp.png" style="width:1.77778in;height:0.88889in" />

## TRANSPORT

The following behavior applies to Internal [*<u>Sync</u>*](\l):

**Rec** enables recording from the Keyboard view or from MIDI in.

**Stop** halts and resets all the Sequencers to step 1.

**Play**/**Pause** works as expected.

Press **Shift** + **Rec** to access **Recording** **Menu**, parameters
from left to right: **Count** **In** before start recording, **Time**
**Quantization** percentage, **Overdub**, recording behavior:
**Looping**, **1**-**Shoot** or **Extend**. With Extend, the length of
the sequence is set when you press stop recording. This behavior allows
to do **MIDI** **Live** **Looping** using external MIDI input or Oxi One
Keyboards.

· Long press **Shift** + **Play** to take a rest from sequencing ;)

During playback, **Shift** + **Stop** activates Punch In falta definir

In Config menu you can choose to enable Rec Listen, this way pressing
Rec arms the track and start recording once a note is activated, being
in One Keyboard or any MIDI input.

**Synchronisation:**

When the OXI One starts up, Sequencer 1 is selected by default and
Internal synchronization mode is set.

The following sync options can be changed by tapping **SHIFT** + **TAP**
button when the sequencer is stopped:

\- **INT**: OXI One follows internal timing, according to the selected
tempo.

\- **USB**: OXI One follows the clock timing received through the MIDI
USB port.

\- **MIDI**: OXI One follows the clock timing received through the MIDI
TRS jack or MIDI BLE.

\- **CLOCK**: OXI One follows the clock timing received through the
clock input.

OXI reads clock according to a 24ppm timing (this can be changed on
[*<u>Configuration</u>*](#configuration)).

<img src="images/image2.png" style="width:2.68611in;height:1.86997in" />

When OXI One is synchronized with a DAW in which either of them act as a
master clock, remember to adjust the **MIDI** **clock** **sync**
**delay** in your DAW settings to correct the latency generated by the
DAW.

## PREVIEW

**Shift** + **Preview** activates playback on the grid Piano Roll view
when you activate steps, this way you get instant feedback through MIDI
and CV if configured of notes and chords you add to the sequences.

## PAGES

Press **16**, **32**, **48** or **64** to select which step page the
grid displays. To access the **80**, **96**, **114** and **128** pages,
hold the corresponding Sequencer button (1-4) and desired page number or
double tap the page number: 16 will show 80, 32 96 and so on, the page
will lit up in blue then.

· Pages from 80 and further are only available in Mono and Chord modes

**Shift + Page** moves the sequence on the grid according to the arrows
displayed. Move the sequence left, up, down and right one step at a time
to **rotate** horizontally or **transpose** vertically.

<img src="images/image3.png" style="width:0.65944in;height:0.33387in" /><img src="images/image4.png" style="width:0.65944in;height:0.33387in" />

## LOAD AND SAVE

OXI One can store **15** **Projects**, each **Project** stores up to
**16** **Patterns** per **Sequencer** (**64** slots in total for any
mode) (change name from slots). To save a sequence hold **Save** button,
**Memory** **Access** view for the current project will show up on the
grid. This layout is independent of the Sequencer you have selected.

<img src="images/image5.png" style="width:7.64974in;height:4.1699in" />

The 4 top rows of pads of the grid shows the Pattern slots per
Sequencer. The slots are saved individually per Sequencer per Pattern,
not the 4 Sequencers at the same time in one Pattern. Yes, the limit is
16 patterns per Project per Sequencer, but by being able of the above,
we can say it’s like having 64 patterns, but that<span
dir="rtl">’</span>s not how the interface works. Instead, there are 16
Patterns so you can give a matrix style co-relation for the different
Sequencers. It helps to tidy up things.

Having 64 patterns is ugly and it’s easy to lose track. This way we have
a better layout structure to address more pattens in an intuitive way,
if you use it in your advantage.

–––––––––––––––––––––

*In OXI One all load events, being Pattern or Project, load up with the
sequencer playing without interruption. Also when possible, the
transport bar keeps its position.*

–––––––––––––––––––––

Press any pad on the row of the **selected** **Sequencer** to save a
sequence in the corresponding Pattern slot.

· You can only save Patterns on the current selected Sequencer (1-4)
row.

· You can save different Sequencer type patterns (Mono, Chord…) in any
slot of the same Sequencer row. Also have the same mode stored in
different Sequencers.

You can also save by turning the encoder 3 to select a Pattern slot in
the screen and press the encoder button.

To load a Pattern hold **Load** button. The Memory access layout will
show up in the grid. You can load 4 patterns (1 per Sequencer) on the
grid INDEPENDENTLY of the selected sequencer (1-4). You can also load by
holding Load button and turning the encoder 3 to select a Pattern slot
in the screen and press the encoder button.

To load a **Project**, hold **Load** button and move encoder 2 to
highlight the target Project, then push the encoder button.

<img src="images/image6.png" style="width:0.65944in;height:0.33387in" /><img src="images/image7.png" style="width:0.65944in;height:0.33387in" />

## COPY AND PASTE

(Review Undo action: Undo, Shift Redo, scrolling both ways by states) In
Oxi One you can copy or paste a Sequencer by holding **Copy** or
**Paste** and Sequencer buttons (1-4), corresponding to the Sequencer
that you want to copy or paste. For Multitrack mode, the active track
will be copied or pasted.

· You can also copy/paste whole Mono tracks into Multitrack tracks by
pressing the first column pad corresponding to the desired track (1-8).
This is very useful to use Mono mode power to compose and Multitrack as
a storage to free up a Sequencer slot.

· Pasting from a Poly sequencer will overwrite the Sequencer type

· Pasting from a Chord sequencer will overwrite the sequencer type
except pasting in Poly, which you can use to fine tune your chord
sequences by note.

· You can copy/paste [*<u>pages</u>*](\l) of 16 steps (32, 48, etc.)
onto another page.

· You can copy/paste single steps with all its parameters

**Shift** + **Copy** duplicates the current sequence’s pages onto the
next ones.

**Shift** + **Paste** deletes all the notes and resets all parameter
values to default on the current Sequencer.

<img src="images/image8.png" style="width:0.65944in;height:0.33387in" />

## UNDO AND REDO

Undo has the last x states stored, press it repeatedly to undo unwanted
takes. Press **Shift** + **Redo** repeatedly to undo the undo… right?

· Step input doesn’t create an undo state

· A new undo state is created with Randomizer, Extend, Clear, Duplicate,
Copy and Paste.

## INIT, END AND LOOP

Change the initial and end step of the active Sequencer or per track in
Multitrack. You can hold Init or End and either press any step on the
grid, turn encoder 2 and 3, or press the pages buttons **16**, **32**,
**48** and **64**. In Mono and Chord modes, double click on the page
buttons to select pages **80**, **96**, **114** and **128**.

**Shift** + **Init** or **End** changes the time division of the
selected Sequencer **x2** or **/2**.

To activate **LOOP**, hold **Init** and **End** buttons and then press
any two steps of two different columns, those will become the looping
points. For Multitrack mode do the same in two steps of the same track
(row). It’ll loop until you press Init and End again. **Loop** will be
still **on** until is manually disabled. When so, the sequence will
continue to play in sync.

–––––––––––––––––––––

*With a LOOP activated, hold the number of the active Sequencer and
press a pad bellow or above the notes played to transpose the LOOP
without affecting the steps outside of it.*

–––––––––––––––––––––

· In Multitrack mode, you can change Init and End steps of all the
tracks at once by holding its Sequencer number button + Init or End and
the desired step.


## DIVISION
<img src="images/image5.bmp.png" style="width:1.77778in;height:0.88889in" />

It activates de Division menu. From left to right:

**Grid** **Vertical** **Scroll**, **Playback** **direction** being
forward, backward, pendulum and random; **Random** **Time** **Division**
percentage, and **Time** **Division**.

The Piano Roll view on the grid is set to the octave selected by
[<u>Oct</u>](\l) and the root note being the lower row. To move the view
up or down to reach higher or lower octaves and further edit steps, use
the Vertical Scroll. When you move vertically through the grid, you’ll
see the rows highlighted, those indicate the position of your root note.
Vertical Scroll is deactivated in Multitrack.

**Random** **Time** **Division** introduces division changes to give
more or less unpredictable rhythms to your sequence. If active, the
Sequencer will be out of sync but it still follows the BPM as clock.

**Shift** + **Division** activates **Extend**. It works as a zoom in on
the sequence: length duplicates and time division halves. Active notes
TIE to following steps to keep its duration.

–––––––––––––––––––––

*Use extend to quickly increase resolution.*

–––––––––––––––––––––

## FOLLOW

Press it to change automatically the displayed page on the grid and to
keep track with the play head. Turn it off to keep the selected page
static so you can make changes in specific pages even with the Sequencer
running.

If Follow is active and you have at least one step pressed, the page
will remain static.

<img src="images/image9.png" style="width:0.65944in;height:0.33387in" />

## RANDOM
<img src="images/image3.bmp.png" style="width:1.77778in;height:0.88889in" />

An easy and musical way to create variation on a sequence is to use
**Random** **Perform**. The parameters affect all the steps per track.
From left to right: Rand **Velocity**, rand **Octave** (In higher values
it adds probability for triggering notes in further octaves), rand
**Probability** of notes being triggered, rand **Retrigger**
probability, which introduces ratcheting effect on steps.

In Multitrack mode instead of Random Octave you have Random Gate, it
doesn’t affect the TIEs.

Random function can be enabled/disabled on individual tracks on
Multitrack mode: the first column will light up orange, press a pad from
the respective track to stop Random from affecting it.

<img src="images/image6.bmp.png" style="width:1.77778in;height:0.88889in" />

**Shift** + **Random** activates the **Random Generator**. It generates
a sequence based on different parameters you can fine tune to come up
with melodic and rhythmic ideas in any [*<u>Sequencer type</u>*](\l).

By holding Random button, you can change: **Random** **scale**;
**Humanize** introduces time offset and velocity variations; **Rand**
adjusts the amount of variation and spread, low values for arpeggios and
high values for less repetitive sequences; **Density** changes the
amount of notes generated. Using the Random Generator is a great way to
come up with new melodies and rhythms.

If you like the notes you have but want a rhythmic change, set **Rand**
to **0** (order) to just randomize the position of the current notes.

## LFO

Another great way to create variation on your sequences is to automate
parameters with the LFOs. There’s one LFO per Sequencer and it can
modify one parameter of the list.

**Waveform**, there’s many to choose from, even unusual shapes.

**Rate** can be clocked up to 2 bars moving the encoder to the left.
Moving to the right will get you to free running LFO.

**Amount**, sets the modification range of the automated parameter. The
bigger, the more drastic changes you will get.

**Destination**, these are: Velocity, Note (quantized to scale), Octave,
Gate, Mod Wheel, Pitch Bend, Time Division, Param (glide on Mono and
Polyphonic modes and strum on Chord mode), Global probability of notes
being played and CC number, this way you can automate parameters in
DAWs, hardware synths or mobile apps.

LFOs can be routed to any CVs out, explained in [*<u>CV Out</u>*](\l)
section.

LFO can be enabled/disabled on individual tracks on **Multitrack** mode:
the first column will light up orange, press a pad from the respective
track to stop LFO from affecting it.

## CONFIGURATION

**Shift** + **Follow** to activate config.

This menu shows global parameters that affects OXI One as a whole. To
navigate through the list, use encoder 2. To change the setting use
encoder 3 and push its button to confirm setting.

Randomize scale and root by default.

Block root and scale during playback. If activated, the scale and the
root won’t be modifiable during playback to avoid unintentional changes.

OXI Split enabled. If activated, MIDI TRS would be compatible with the
OXI Split increased bandwidth.

Send MIDI Clock. If activated, MIDI clock will be sent through TRS, USB
and Bluetooth.

Ignore MIDI Start Stop. If activated, OXI One transport won’t be
controlled externally.

PPQ MIDI Out. Changes the pulses per quarter note OXI One sends through
MIDI.

PPQ MIDI In. Changes the pulses per quarter note OXI One receives
through MIDI.

Send CC of muted tracks. If activated, muted tracks will still send CC
information.

TRS MIDI Thru. If activated, OXI will echo the MIDI messages received
through TRS.

USB MIDI Thru. If activated, OXI will echo the MIDI messages received
through USB.

MIDI In channel filtering. If activated, OXI one

MIDI scale note filtering.

PPQ Clock Out. Changes the pulses per quarter note OXI One sends through
analog Clock Out.

MIDI CC Smoothing. If activated, CC values will be interpolated between
consecutive steps. If deactivated, CC values will be sent per step
(stepped behavior).

MIDI CC Smooth Factor. Changes the response curve of the interpolated CC
values, 1 being

Keyboard Arpeggiator velocity per note played

Loop Sync on release. Changes the behavior of the [<u>LOOP</u>](\l)
function when it’s released, SYNC BAR: the sequence will continue on the
next bar; SYNC BEAT, the sequence will continue on the next beat or
step; NO SYNC, the sequence will continue exactly when it’s released.

Reset track length when clearing. If activated, the Sequencer or track
length will be reseted to 16 steps when pressing [<u>Clear</u>](\l).

Reset track time division when clearing. If activated, the Sequencer or
track time division will be reseted to 1/16 when pressing
[<u>Clear</u>](#FIXME).

Pause enabled. If activated, OXI One transport will have Pause/Play
behavior, if not, only Play.

# MODES

<img src="images/image10.png" style="width:0.56423in;height:0.33387in" />

<img src="images/image7.bmp.png" style="width:1.77778in;height:0.88889in" />

## KEYBOARD

It shows the keyboard layout on the grid. Each Sequencer type has its
own dedicated Keyboard layout. Different columns of pads will be
highlighted on the grid depending on the scale selected, to act as a
visual guide. Each column represents a note, moving vertically jumps an
octave up and down.

When a scale is selected, its notes will be bright on the keyboard to
give instant visual guide to play in tune.

The encoders in this mode modifies, from left to right: **Velocity**,
**Pitch** **Bend**, **Pressure** (aftertouch)/glide and **Mod**
**Wheel** or the last active **CC**. (add vel rand or LFO)

Press **Rec** to record notes when **Play** is active. You can adjust
quantization and other parameters pressing [***<u>Shift +
Rec</u>***](\l).

### ARP
<img src="images/image8.bmp.png" style="width:1.77778in;height:0.88889in" /><img src="images/image11.png" style="width:0.56423in;height:0.33387in" />

In Keyboard view, press **Arp** to enter the arpeggio menu. Each
Sequencer has one arpeggio that can be playing independently of what is
playing on the Sequencer. To activate it, turn encoder 1 to choose the
arpeggio mode, encoder 2 for octave range (positive values changes
octave up, negative values change octave up and down) encoder 3 for gate
length and encoder 4 for time division.

· Arpeggio only works when **Play** is on, independently if the
Sequencer is

active or not.

Press **Rec** to record the arpeggio on the Sequencer (it does not work
for arpeggiated chords in Chord mode).

–––––––––––––––––––––

*In a Polyphonic Sequencer and with overdub on,*

*you can record several layers of the arpeggios played.*

–––––––––––––––––––––

**Shift** + **Arp** activates **Hold**. This way you can add and remove
notes of the arpeggio without having to hold them manually. If an
arpeggio is held, exiting the Keyboard view or changing Sequencer won’t
turn it off, this way you can have 4 arpeggios held at the same time.

We’ll explore specific features on Keyboard for [*<u>Chord</u>*](\l) and
[*<u>Multitrack</u>*](\l) modes on their section.

## CV OUT

**Shift** + **LFO** opens the **CV** **Out** grid layout.

This mode is shared by all Sequencers to avoid overlapping routings.

There’re 8 cvs and 8 gates that can be fully configurable on real time
with the Sequencers on, this means you can use this layout as an
innovative tool for performance. You can turn on/off outputs, change
voice or Sequencer, modulation source or clock division with the press
of a button. The diagram bellow explains how the layout it’s
distributed.<img src="images/image2.jpeg" style="width:8.26778in;height:3.87939in" />

### Sequencer selection

The first 4 top rows selects which Sequencer is providing the modulation
source, 1 to 4. Pressing on the active pad will turn off the
corresponding output. Use this to create transitions in your
performances adding and removing voices and rhythms.

### Modulation selection

Each [*<u>Sequencer type</u>*](\l) has a different amount of voices
available that you can route to the CVs and Gates:

· [*<u>Mono</u>*](\l) 1 voice

· [*<u>Chord</u>*](\l) up to 8 voices

· [*<u>Polyphonic</u>*](\l) up to 7 voices

· [*<u>Multitrack</u>*](\l) up to 8 voices (1 per track)

**Pitch**, **Gate**, **Velocity** and [**<u>Trig Envelopes</u>**](\l)
modulation sources are correlated by **voice** **number**. After
selecting one of these sources you can choose which voice it reads from.

To carry a typical modular voice you need to choose the same Sequencer
and voice number for Pitch and Gate or Trig Envelope.

This method allows you to choose the amount of voices and which CVs do
you want to route them through. Sequencing chords and polyphonic
melodies on your modular synth is very straightforward and as flexible
as it can get, you can select the number of voices you want to output.
In case of having a less voices routed than the total available for
Polyphonic and Chord modes, the voices will be sorted by the last
played.

### Trig Envelope

In Oxi One we have introduced Trig Envelopes, an Attack-Decay envelope
that changes dynamically with MIDI Velocity and Gate. The amplitude of
the envelope is proportional to MIDI velocity and its Decay is
proportional to MIDI gate length. The Attack can be modified. This
implementation tends a bridge between MIDI and CV messages, by having a
dynamic envelope ready to modulate parameters of your modular synth like
VCA, LPG, LPF and any other fun acronyms on your rack.

–––––––––––––––––––––

*Apply modulation and/or randomization to Gate and Velocity*

*to also modulate Amplitude and Decay of the Trig Envelope.*

–––––––––––––––––––––

OXI One does **MIDI** **to** **CV** conversion from external MIDI being
TRS, USB or Bluetooth.

To do so, just configure the [<u>MIDI channel of the Sequencer</u>](\l)
routed to the CV and Gate outputs you want to use.

To convert external (MIDI in) or internal (OXI One) CC messages to CV,
configure the desired CV output in the bottom row of CV Out layout,
where you can select the internal LFO or Mod number as modulation
source. Mod is linked to a CC number you can select. To know which CC
number corresponds to each Mod see [**<u>CC Configuration</u>**](\l).

CVs and gates come calibrated, but if you need to recalibrate, please
follow the instructions on
[**<u>Calibration</u>**](\l).<img src="images/image12.png" style="width:0.56333in;height:0.33333in" />

## ARRANGER

This is the place to create your Songs, link several patterns or launch
them
manually.<img src="images/image3.jpeg" style="width:1.77778in;height:0.88889in" />

To activate the Arranger playback hold **Shift** + **Arranger**, you’ll
se “Arranger ON” on the screen.

The Arranger is then ready for playback. Turn OFF Arranger from playing
with the same command.

The bottom 4 rows of the grid are a 16 position sequence for each
Sequencer respectively, so you have 4 lanes of pattern playback. The top
4 rows are the Memory Access where you can see the saved patterns of the
4 Sequencers. To add a pattern on the arranger, simply hold its slot pad
and press a pad in the desired position of the corresponding Sequencer
lane of the arranger.

Hold a pad of the arranger to see the parameters of the pattern step,
that is **Program Change**, **Color** changes the color for all the pads
that plays the same stored pattern so you can easily spot them,
**Pattern** being played and **Repeats** of that pattern in that
position, up until **infinite** repeats of the pattern (after 19 you
will see the infinite icon displayed).

–––––––––––––––––––––

*To set the Arranger ready for playback press **Shift** + **Arranger**,
you’ll se “Arranger ON” on the screen. The Arranger then ready for
playback. Turn OFF Arranger from playing with the same combination of
buttons.*

–––––––––––––––––––––

On the main screen you can see on the right the countdown for each
arrangement track pattern to change to the next one and a progress bar
of it. If the current pattern is looped a round arrow will indicate
this.

**Songs** are saved states of the Arranger, that means you can save and
recall structures of sequenced patterns per Project. Any unsaved changes
to the patterns being played won’t be saved on your Songs.

### Clip Launch

Hold Shift and press one pad per arrangement track to launch that
pattern when the current one ends. This is helpful to have patterns
looping until you want to launch a new part of your song in real time.

## CC PERFORM

Press the 4th encoder button to enter **CC Perform** mode. This mode is
independent and configurable per Sequencer. On it, the encoders send
values over MIDI CC values, so you can control any external parameters
with the 4 encoders of any DAW, hardware synthesizer or sampler, modular
synth or mobile apps.

In this mode, press any of the encoder buttons turn OFF that CC,
preventing it from sending messages, the CC will keep its current value.
Twist the encoder to turn ON and jump to the previous value.

When exiting the CC Perform screen, the last used encoder will be the
one that the main [**<u>Mod</u>**](\l) control in the encoder 4 for
**[<u>Sequencer screen</u>](\l)** changes. This allows to edit the value
per step as with velocity, gate, etc. marcador a step-chord

Long press encoder 4 to access **CC** **Configuration**, here you can
select which CC number each encoder modifies **Mod 1-4**. Each encoder
corresponds The set of CCs is different per Sequencer, up to 32 CCs in
total.

### Motion Recording

The value changes of the CC values can be recorded in real time by
pressing **Rec** button on CC Perform mode. Play along the sequence to
record the movement of the encoders. This will create a looping
automation envelope as long as your sequence goes.

Moving the encoder offsets the range of the motion: if the range was
30-100, turning the encoder 20 to the left will result in a 10-80 rang

Same as above, press any of the encoder buttons turn OFF that motion,
the CC value . Twist the encoder to turn ON and continue the motion.

In Multitrack mode, each track has 1 CC available, making a total of 8
that can be recorded.

The CC automation can be routed to any CV output (see [<u>CV
Out</u>](\l) configuration)

The CC automation is set by step. To avoid having jumps on values, the
CCs are smoothed with interpolated values. The curve that defines the
smoothing can be adjusted in [<u>configuration</u>](\l).

# SEQUENCER TYPES

**METER STEP - CHORD PARAMS AQUÍ**

## MONO

Mono mode is a great place to create the leads of your song, perform
over it or automate it.

In Mono mode, every row represents the note position according to the
selected scale, starting from the selected root note. For example, in
the C Major scale, steps would be C, D, E, F, G, A, B and C in a higher
octave.

There’s up to 128 steps, review [*<u>Init and End</u>*](\l) function
buttons to recall how to change the length.

## CHORD

In Chord mode, every grid row represents by default, each degree of the
selected scale. For example, in the C Major scale, from bottom to top
the rows would represent Cmaj, Dmin, Emin, Fmaj, Gmaj, Amin, Bdim.

Multi afectar a todas las pitas pulsando el boton de seq (clear,
randomizer…)

\- Keyboard: 1st and 2nd columns 4 lower pads select the chord given the
scale and the root note played. 2nd column top 4 pads select the strum
duration. 3rd and 4th selects the inversion.

### Chord editting

Chord sequence is one type of sequence which helps you to easily
introduce chords into the pattern. You can create from simple triads
progressions to complex chord progressions with all kind of tensions,
inversions and all that related to the scale degrees.

In a chord sequence: every row represents, by default, each degree of
the selected scale. For example, in the C Major scale, from bottom to
top, the rows would represent Cmaj, Dmin, Emin, Fmaj, Gmaj, Amin, Bdim.

We can change the chord of every step in the chord menu. In order to do
so, press the Step-Chord button and press the desired active step button
and the chord menu for this step will show up. The default chord is a
triad of the corresponding degree. The inversions are set for every
degree in order the chord voices are more musically balanced.

The second knob or Oct knob sets the chord and the third knob or gate
knob, sets the inversion or voicing. You can modify several steps at
once but only the last pressed will be showed.

Chords are ordered from less to more complexity (with complexity we mean
the number of voices and tensions). Suspended 2, suspended 4 chords and
fifths and octave intervals appear after. Finally, we reach the
Modulation option. From this point, the chord is modulated changing from
a Major to a Minor chord or viceversa depending on its default nature.

## MULTITRACK
Every row represents a separate monophonic sequence
with independent Init/End steps. They can be used to trigger a Drum Kit
or samples in general, or even extra melodic lines. This means several
steps can be activated per column.

\- Keyboard: 2nd column selects arm track. Without any selection and by
default, the 8 highlighted pads plays each track (drum pad). If you
select one track in this column, the keyboard will play as a regular
keyboard on that track (remember MIDI channel is per track). 3rd column
is Mute per track. 4th column is Solo per track.

## POLYPHONIC

## HARMONICER

Harmonizer mode is a scale setting for Mono, Polyphonic and Multitrack
Sequencer types, in which the pitch of the notes is taken from the
active chord on a Chord Sequencer instead of the Sequencer itself. This
works as follows: the rhythm and the pitch trend is set on the Piano
Roll of the selected Sequencer, but the final pitch is based from the
active chord progression.

In this mode you are not only in key, but any changes applied to the
chord sequencer will affect the rest of the sequencers with Harmonizer
on, for example, changing the scale, transposing the chord sequence, or
playing directly on the powerful Chord Keyboard.

# APP

Firmware update, backups…

# MIDI & CLOCK

· **MIDI TRS** explicar conversión entre trs usb y ble y a cv

# MONOME GRID

# CALIBRATION

To enter calibration mode, press shift + cv gate during more than 2
seconds.

Octaves: from C-1 to C7

Values that OXI Should output in each CV output :D

C-1: -3V

C0 : -2V

C1 : -1V

C2 : 0V

C3 : 1V

C4 : 2V

C5 : 3V

C6 : 4V

C7 : 5V

Every point has to be calibrated for each CV.

For example, if pressing C2, OXI shows 0,123V, the CV is uncalibrated in
the C2 point by an offset of 0.123V

To adjust it down you should press the immediate key below C2 which is
B1.

To adjust it up, you should press the immediate key above C2 which is
C\#2.

(If you press A\#1 or D2 the jump is bigger to speed up things)

Press it repeatedly until the voltage offset is +-0.005V off the
expected value.

Once you have finished with all the points of the calibration curve,
press C8 to save.

You can do it with any keyboard connected to the MIDI IN port, but we
recommend to do it in your DAW to better see the octave range.

Every CV listens to a different MIDI channel ,CV1 listens to MIDI
Channel 1 and CV2, to the MIDI channel 2 and so on, until CV8 in channel
8.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Multi: Step + random = rand vel of track

MONO and CHORD: transpose seq button + pad column

Sequence Parameters 1

Every step has different parameters:

Velocity: the MIDI velocity that is sent, and also the amount of CV
velocity signal. We can modify the velocity of each step by pressing the
step and turning the VEL knob.(This applies to other parameters as well,
and it's the reset function of the knobs).

Octave: the octave modulation of every step, that is added (or
subtracted) to the sequence octave. We can modify the velocity of each
step by pressing the step and turning the OCT knob.

Gate: this parameter sets the duration of the note, determined by the %
of the total length of every step depending on the selected division.
Thus, if the sequence is set to 1/16th, and gate is 50, the note will
last half of a 1/16th note. We can tie an step by setting the gate value
of the previous step to the maximum: TIE. Tied steps are indicated with
a change in their color. The steps must be on the same row and octave to
be tied, otherwise they would be considered as different notes.

We can also TIE various steps at once by pressing the first step and the
last step of the tied note in the SAME row. We have to wait a little bit
before the second press (half of a second) in order to OXI ONE
recognizes our gesture.

Modulation: sets the MIDI modulation that is sent with every step.

We can also globally modify these parameters by turning the knobs
without pressing any button step of the grid, the OLED screen will show
the minimum and maximum values of the corresponding parameter in the
selected sequence. By turning the OCT knob we change the global octave
of the sequence, and it is displayed in the OLED screen. The notes go
from C-2 to C7.

Sequence Parameters 2

There are further step parameters that can be modified. These are
accesed by pressing the Step/Chord button (currently named Note-Chord).

Offset: moves the step before or after the exact grid position. The
value indicates the % regarding the division length.

Repeat: number of times that the step is retriggered in equally amount
of time. For example, if the division is set to 1/16 and retrigger is
set to 2, two notes will be triggered at 1/32th division. The gate
length will be then, half of the normal length.

Probability: it represent the % probability that the step is triggered.
100% allways triggered, 0% never triggered.

The fourth parameter depends on the sequence type.

In a mono sequence it will set the glide amount for the assigned CV
output of the current step.

In a chords sequence it will set the strum value of the selected chord.
Positive value would mean a strum from low to high note, and viceversa.

In a drums sequence: it will set the midi note offset of the selected
step (not quantised to any scale). ??

In a drums sequence: it will set the midi channel offset of the selected
row ??

If we are in a chords sequence and we press Step/Chord button ONCE, we
access the chords menu where we can selected the desired chord and
inversion from the list. More info about chord selection in the Chord
section.

If we are in a chords sequence and we press Step/Chord button TWICE, we
access the parameters above described.

We can continue with this sequence it if we like, we can press RANDOM to
generate a new randomised sequence or we can start with a new one by
pressing CLEAR \[SHIFT + LOAD\]. Check more about this in the
corresponding sections.

Sequence Length

We can change the Start and End steps in different ways:

By pressing the INIT or END button + one STEP button of the grid.

Pressing the INIT or END button and turning the OCT or GATE knobs.

By pressing INIT/END and any of the 4 part buttons: 16, 23, 48 or 64

Note the init step can never go further than the end step and viceversa,
in that case the Init step is equaled to the End step or viceversa. The
minimum sequence length is 1 step.

Sequence Launch

We activate and deactivate sequences by pressing SHIFT and the desired
sequence.

What happens if the OXI ONE is running and we activate on sequence? The
sequence will start in the following beat according to a beat
synchronisation. (The synchronisation time base can be modified in
future versions to bar sync, 4 bar sync or no sync with the app/setup
menu).

What happens if the OXI ONE is running and we deactivate on sequence?
The sequence will stop immediately.

It is possible to activate/deactivate several sequences at once.

Clear sequence

A sequence can be totally cleared and reseted by pressing SHIFT + CLEAR

Duplicate sequence

A sequence can be duplicated by pressing SHIFT + DUPLICATE. This will
double the length of the sequence by duplicating its active steps.

For example, if we start from a 16 step length sequence, we would get a
32 step length sequence where step 1 is equal to step 17, step 2 is
equal to step 18 and so on.

If the sequence is for example 40 steps long, only the remaining 24
steps would be copied until the maximum length of 64 steps.

Randomizer

There are two randomization menus. One is dedicated to generate new
sequences or musical ideas and the second one is meant to set the amount
of variation applied to every sequence during its playback.

The first one is accesed by pressing SHIFT + RANDOM buttons. We have
several creative randomization options and features to generate a new
randomized sequence or musical idea.

Root and scale randomization: the root note and scale will be randomized
or not according to this parameter: ON/OFF.

Humanization: velocity, octave and gate are randomization to give more
natural or human feel with slights amounts of it or to get more radical
results with greater values.

Randomization ammount: the less of this factor, the more similar musical
phrases we will get in our new sequence.

Note density: probability of having more or less active steps. 0 is no
active steps, 100 is full active steps.

Note that Init/End step, division, won't be touched by the randomization
motor.

A new randomization is applied every time we release the RANDOM button.
We can always press UNDO to get to our previous sequence. There is only
one undo step currently supported.

In the second menu, the Random Perform, you can set the amount of
variation that is applied to:

\- Velocity affecting every step of the sequence.

\- Octave of the steps of the sequence.

\- Global probability (per sequence) of steps triggering (it is 100%
with no randomization). Note there is also a per step trigger
probability, we'll cover this later.

\- Retrigger probability, from 1 (no retrigger) to 4 retrigger maximum
per step for all the sequence.

Step parameter submenu.
